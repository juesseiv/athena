# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

from AthenaPoolTest.EventInfoReadWrite import eventInfoTestCfg
import sys

sys.exit(eventInfoTestCfg(5))
