/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MuonHitTesterAlg.h"
DECLARE_COMPONENT(MuonValR4::MuonHitTesterAlg);