
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/


#include "../xAODSimHitToMdtMeasCnvAlg.h"
#include "../xAODSimHitToRpcMeasCnvAlg.h"
#include "../xAODSimHitToTgcMeasCnvAlg.h"
#include "../xAODSimHitTosTGCMeasCnvAlg.h"
#include "../xAODSimHitToMmMeasCnvAlg.h"

DECLARE_COMPONENT(xAODSimHitToMdtMeasCnvAlg)
DECLARE_COMPONENT(xAODSimHitToRpcMeasCnvAlg)
DECLARE_COMPONENT(xAODSimHitToTgcMeasCnvAlg)
DECLARE_COMPONENT(xAODSimHitTosTGCMeasCnvAlg)
DECLARE_COMPONENT(xAODSimHitToMmMeasCnvAlg)