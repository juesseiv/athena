/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "TrackRecord/TrackRecord.h"


#include "G4SimTPCnv/TrackRecord_p1.h"
#include "G4SimTPCnv/TrackRecordCnv_p1.h"
#include "TruthUtils/MagicNumbers.h"


void
TrackRecordCnv_p1::persToTrans(const TrackRecord_p1* persObj, TrackRecord* transObj, MsgStream &log) const
{
   log << MSG::DEBUG << "TrackRecordCnv_p1::persToTrans called " << endmsg;

   transObj->SetPDGCode(persObj->PDG_code());
   transObj->SetEnergy((double) persObj->energy());
   transObj->SetMomentum(CLHEP::Hep3Vector(persObj->momentumX(), persObj->momentumY(), persObj->momentumZ() ));
   transObj->SetPosition(CLHEP::Hep3Vector(persObj->positionX(), persObj->positionY(), persObj->positionZ() ));
   const int oldStatus = 1; // Given how TrackRecords are used currently; this will be correct for all but some very exotic samples.
   transObj->SetStatus(HepMC::new_particle_status_from_old(oldStatus, persObj->barCode()));
   transObj->SetTime((double) persObj->time());
   transObj->SetBarcode(persObj->barCode()); // FIXME barcode-based
   transObj->SetVolName(persObj->volName());
}


void
TrackRecordCnv_p1::transToPers(const TrackRecord* transObj, TrackRecord_p1* persObj, MsgStream &log) const
{
   log << MSG::DEBUG << "TrackRecordCnv_p1::transToPers called " << endmsg;
   persObj->m_PDG_code = transObj->GetPDGCode();
   persObj->m_energy = (float) transObj->GetEnergy();
   CLHEP::Hep3Vector mom = transObj->GetMomentum();
   persObj->m_momentumX = (float) mom.x();
   persObj->m_momentumY = (float) mom.y();
   persObj->m_momentumZ = (float) mom.z();
   CLHEP::Hep3Vector pos = transObj->GetPosition();
   persObj->m_positionX = (float) pos.x();
   persObj->m_positionY = (float) pos.y();
   persObj->m_positionZ = (float) pos.z();
   persObj->m_time = (float) transObj->GetTime();
   persObj->m_barCode = HepMC::barcode(transObj); // FIXME barcode-based
   persObj->m_volName = transObj->GetVolName();
}

